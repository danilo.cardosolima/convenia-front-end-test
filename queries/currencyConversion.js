export const currencyConversion = (currency) => {
  return `
  {
    currencyConversion(baseCurrency: "BRL", convertCurrencies: "${currency}") {
      conversions {
        rate,
         currencyInfo {
          code
          symbol
        }
      }
    }
  }
  `;
};
