import { mount } from "@vue/test-utils";

import OutputPanel from "@/components/OutputPanel.vue";
import LeTipTotal from "@/components/LeTipTotal.vue";

const defaultProps = {
  propsData: {
    currency: {
      code: "USD",
      rate: 0,
      symbol: "$",
    },
    totalAccount: 0,
    totalBRL: 0,
    totalFinal: 0,
    totalPerPerson: 0,
    totalTip: 0,
  },
  stubs: {
    shallow: true
  }
}

describe('OutputPanel - unit', () => {
  it('should mount the component', () => {
    const wrapper = mount(OutputPanel, defaultProps)

    expect(wrapper.vm).toBeDefined();
  });

  it('should render LeTipTotal component on mount', () => {
    const wrapper = mount(OutputPanel, defaultProps)

    expect(wrapper.findComponent(LeTipTotal)).toBeDefined();
  });

  it('should find text on rendered template', () => {
    const wrapper = mount(OutputPanel, defaultProps)

    expect(wrapper.html()).toContain('Conta');
    expect(wrapper.html()).toContain('Gorjeta');
    expect(wrapper.html()).toContain('Total');
    expect(wrapper.html()).toContain('por Pessoa');
    expect(wrapper.html()).toContain('em R$');
  });
});
