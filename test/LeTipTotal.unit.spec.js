import { mount } from "@vue/test-utils";

import LeTipTotal from "@/components/LeTipTotal.vue";

describe('LeTipTotal - unit', () => {
  it('should mount component', () => {
    const wrapper = mount(LeTipTotal, {
      propsData: {
        symbol: '',
        value: '',
      }
    });

    expect(wrapper.vm).toBeDefined();
  });

  it('should throw error if required props are falsy', () => {
    const wrapper = mount(LeTipTotal, {
      propsData: {
        symbol: 'R$',
        value: 73.23,
      }
    });

    expect(wrapper.props().symbol).toBeTruthy();
    expect(wrapper.props().value).toBeTruthy();
  });

  it('should find label text if label props is defined', () => {
    const wrapper = mount(LeTipTotal, {
      propsData: {
        label: 'label test',
        symbol: 'R$',
        value: 73.23,
      }
    });

    expect(wrapper.text()).toContain('label test');
  });
})
