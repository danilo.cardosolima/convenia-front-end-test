import { mount } from "@vue/test-utils";

import InputSwitch from "@/components/InputSwitch.vue";

describe('InputSwitch - unit', () => {
  it('should mount component', () => {
    const wrapper = mount(InputSwitch);

    expect(wrapper.vm).toBeDefined();
  });

  it('should render props with default values', () => {
    const wrapper = mount(InputSwitch);

    expect(wrapper.props().value).toBe(false);
    expect(wrapper.props().activeLabels).toBe(false);
  });

  it('should emit input event when switch is clicked', async () => {
    const wrapper = mount(InputSwitch);

    await wrapper.find('#checkbox').trigger('input');

    expect(wrapper.emitted().input).toBeTruthy();
  });

  it('should update value when switch is clicked', async () => {
    const wrapper = mount(InputSwitch);

    await wrapper.find('#checkbox').setChecked();
    await wrapper.find('#checkbox').trigger('input')

    expect(wrapper.emitted().input[0][0]).toBe(true);
  });

  it('should update value to unchecked when switch is clicked', async () => {
    const wrapper = mount(InputSwitch, {
      propsData: {
        value: true
      }
    });

    await wrapper.find('#checkbox').setChecked(false);
    await wrapper.find('#checkbox').trigger('input')

    expect(wrapper.emitted().input[0][0]).not.toBe(true);
  });

  it('should find default text if activeLabels is true, default is off and true to labels', () => {
    const wrapper = mount(InputSwitch, {
      propsData: {
        value: true,
        activeLabels: true,
      }
    });

    expect(wrapper.text()).toContain('off');
    expect(wrapper.text()).toContain('on');
  });


  it('should find custom text if activeLabels is true and use slots to customize labels', () => {
    const wrapper = mount(InputSwitch, {
      propsData: {
        value: true,
        activeLabels: true,
      },
      slots: {
        unchecked: '<div class="input-switch__label">EUR</div>',
        checked: '<div class="input-switch__label">USD</div>',
      }
    });

    expect(wrapper.text()).toContain('EUR');
    expect(wrapper.text()).not.toContain('off');
    expect(wrapper.text()).toContain('USD');
    expect(wrapper.text()).not.toContain('on');
  });

})
