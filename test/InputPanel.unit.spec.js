import { mount } from "@vue/test-utils";

import InputPanel from "@/components/InputPanel.vue";
import InputRange from '@/components/InputRange.vue';
import InputSwitch from '@/components/InputSwitch.vue';
import InputValue from '@/components/InputValue.vue';


const defaultProps = {
  propsData: {
    currency: {
      code: "USD",
      rate: 0,
      symbol: "$",
    },
    account: {
      personQuantity: 2,
      tipPercent: 10,
      value: 0,
    },
    switchChecked: false
  }
}

let props;

describe('InputPanel - unit', () => {

  beforeEach(() => {
    props = defaultProps;
  });

  it('should mount the component', () => {
    const wrapper = mount(InputPanel, props);

    expect(wrapper.vm).toBeDefined();
  });

  it('should render InputRange component on mount', () => {
    const wrapper = mount(InputPanel,  props)

    expect(wrapper.findComponent(InputRange)).toBeDefined();
  });

  it('should render InputSwitch component on mount', () => {
    const wrapper = mount(InputPanel,  props)

    expect(wrapper.findComponent(InputSwitch)).toBeDefined();
  });

  it('should render InputValue component on mount', () => {
    const wrapper = mount(InputPanel,  props)

    expect(wrapper.findComponent(InputValue)).toBeDefined();
  });

  it('should modify data binds on template', () => {
    props.propsData.account = {
      personQuantity: 999,
      tipPercent: 300,
      value: 0,
    }

    props.stubs = {
      shallow: true
    }

    const wrapper = mount(InputPanel, props);

    expect(wrapper.html()).toContain("999");
    expect(wrapper.html()).toContain("300%");
  });

});
