import { mount } from "@vue/test-utils";

import InputValue from "@/components/InputValue.vue";

describe('InputValue - unit', () => {
  it('should mount the component', () => {
    const wrapper = mount(InputValue, {
      propsData: {
        value: '',
        label: ''
      }
    });

    expect(wrapper.vm).toBeDefined();
  });

  it('should throw error if required props is undefined', () => {
    const wrapper = mount(InputValue, {
      propsData: {
        value: 73.23,
        label: 'Valor'
      }
    });

    expect(wrapper.props().value).toBeTruthy();
    expect(wrapper.props().label).toBeTruthy();
  });

  it('should update emit input when user typing value', async () => {
    const wrapper = mount(InputValue, {
      propsData: {
        value: 73.23,
        label: 'Valor'
      }
    });

    await wrapper.find('.input-value').setValue(74.24);
    const value = parseFloat(wrapper.emitted().input[0][0]);

    expect(value).toEqual(74.24);
  });

  it('should include symbol element if symbol prop is defined', () => {
    const wrapper = mount(InputValue, {
      propsData: {
        value: 73.23,
        label: 'Valor',
        symbol: 'R$'
      }
    });

    expect(wrapper.text()).toContain('R$');
  });
});
