import { mount } from "@vue/test-utils";

import InputRange from "@/components/InputRange.vue";

describe("InputRange - unit", () => {
  it("should mount the component", () => {
    const wrapper = mount(InputRange, {
      propsData: {
        id: "test",
        label: "test",
      },
    });

    expect(wrapper.vm).toBeDefined();
  });

  it("should pass if label is rendered", () => {
    const wrapper = mount(InputRange, {
      propsData: {
        id: "test",
        label: "testing label",
      },
    });

    expect(wrapper.text()).toContain("testing label");
  });

  it("should return selector if id is inputed", () => {
    const wrapper = mount(InputRange, {
      propsData: {
        id: "testing",
        label: "",
      },
    });

    expect(wrapper.find("#testing")).toBeTruthy();
  });
});
