import { mount } from "@vue/test-utils";

import ButtonNav from "@/components/ButtonNav";

describe('ButtonNav - unit', () => {

  it('should mount the component', () => {
    const wrapper = mount(ButtonNav);

    expect(wrapper.vm).toBeDefined();
  });

  it('should include icon (→) when prop icon is default', () => {
    const wrapper = mount(ButtonNav);

    expect(wrapper.text()).toBe('→');
  });

  it('should include icon when prop icon is not default', () => {
    const wrapper = mount(ButtonNav, {
      propsData: {
        icon: '>'
      }
    });

    expect(wrapper.text()).toBe('>');
  });

  it('should emit click event when pressed', () => {
    const wrapper = mount(ButtonNav);

    wrapper.find('button').trigger('click');

    expect(wrapper.emitted().click).toBeTruthy();
  });
});
