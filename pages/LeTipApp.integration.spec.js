import { mount } from "@vue/test-utils";
import axios from "axios";

import LeTipApp from ".";
import InputPanel from "@/components/InputPanel"
import OutputPanel from "@/components/OutputPanel"
import { currencyConversion } from "@/queries/currencyConversion";

axios.defaults.baseURL = "http://localhost:8000";

const query = currencyConversion("USD");

jest.mock("axios");

describe("Le Tip App - integration", () => {
  it("should mount the component", async () => {
    const wrapper = await mount(LeTipApp, {
      mocks: {
        $axios: axios,
      },
    });

    expect(wrapper.findComponent(InputPanel)).toBeDefined()
    expect(wrapper.findComponent(OutputPanel)).toBeDefined()

    expect(wrapper).toBeDefined();
  });

  it('should request when component is mounted to baseUrl/graphql passing query', async () => {
    const wrapper = await mount(LeTipApp, {
      mocks: {
        $axios : axios
      }
    });

    expect(wrapper).toBeDefined();
    expect(axios.post).toHaveBeenCalledWith("/graphql", { query });
  });
});
